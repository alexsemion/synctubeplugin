<?php
define('HTTP_INVALID_REQUEST', 'HTTP_INVALID_REQUEST');

define('PLAYER_DOCUMENT_NAME', 'player-document.json');
define('CHAT_DOCUMENT_NAME', 'chat-document.json');

define('PUSHER_APP_ID', '3e206b6b3bea4d4bc2ff');
define('PUSHER_APP_KEY', '95836f4c9eda75f4f4ad');
define('PUSHER_APP_SECRET', '390049');

define('PUSHER_VIDEO_CHANNEL', 'video');
define('PUSHER_CHAT_CHANNEL', 'chat');

define('PUSHER_VIDEO_BROADCAST_END_EVENT', 'video:broadcast.end');
define('PUSHER_VIDEO_PLAY_EVENT', 'video:play');
define('PUSHER_VIDEO_CURRENT_TIME_EVENT', 'video:current.time');

define('PUSHER_CHAT_USER_EVENT', 'chat:user');
define('PUSHER_CHAT_MESSAGE_EVENT', 'chat:message');
define('PUSHER_CHAT_CLEAR_HISTORY_EVENT', 'chat:clear-history');

function routeRequest(){
  if(empty($_GET['method'])){
    return ;
  }

  // Execute requested method
  $_GET['method']($_POST);
}

function successHttpResponse($data){
  http_response_code(200);
  echo json_encode($data);
}

function errorHttpResponse($code){
  http_response_code(400);
  echo json_encode(['code'=>$code]);
}

function getDocumentContent($documentName, $emptyDocument){
  if(!file_exists($documentName)){
    file_put_contents($documentName, json_encode($emptyDocument));
    $document=$emptyDocument;
  }
  else{
    $document=file_get_contents($documentName);
    
    if(empty($document)){
      file_put_contents($documentName, json_encode($emptyDocument));
      $document=$emptyDocument;
    }
    else{
      $document=json_decode($document, true);
    }
  }

  return $document;
}

function saveDocumentContent($documentName, $document){
  file_put_contents($documentName, json_encode($document));
}

// Player document methods

function getPlayerEmptyDocument(){
  return ['event'=>null, 'currentTime'=>0];
}

function getPlayerDocumentContent(){
  return getDocumentContent(PLAYER_DOCUMENT_NAME, getPlayerEmptyDocument());
}

function savePlayerDocumentContent($document){
  return saveDocumentContent(PLAYER_DOCUMENT_NAME, $document);
}

function truncatePlayerDocument(){
  return savePlayerDocumentContent(getPlayerEmptyDocument());
}

// Chat document methods

function getChatEmptyDocument(){
  return ['users'=>[], 'messages'=>[]];
}

function getChatDocumentContent(){
  return getDocumentContent(CHAT_DOCUMENT_NAME, getChatEmptyDocument());
}

function saveChatDocumentContent($document){
  return saveDocumentContent(CHAT_DOCUMENT_NAME, $document);
}

function truncateChatDocument(){
  return saveChatDocumentContent(getChatEmptyDocument());
}

function arrayToCsv(array $fields, $delimiter=',', $enclosure='"', $encloseAll=false){
  $delimiter_esc=preg_quote($delimiter, '/');
  $enclosure_esc=preg_quote($enclosure, '/');

  $output=[];
  foreach($fields as $field){
    if($field===null){
        $output[]='NULL';
        continue ;
    }

    // Enclose fields containing $delimiter, $enclosure or whitespace
    if($encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)){
      $output[]=$enclosure.str_replace($enclosure, $enclosure.$enclosure, $field).$enclosure;
    }
    else{
      $output[]=$field;
    }
  }

  return implode($delimiter, $output);
}
<?php
ini_set('display_error', true);
ini_set('error_reporting', E_ALL);

require_once '../vendor/autoload.php';
require_once './methods.php';

require_once './chat.php';

routeRequest();

/********** API methods **********/

function pushEvent($request){
  if(empty($request['event'])){
    return errorHttpResponse(HTTP_INVALID_REQUEST);
  }

  $event=$request['event'];
  $data=isset($request['data']) ? $request['data'] : null;

  saveEvent($event, $data);
  
  $options=['cluster'=>'eu', 'encrypted'=>true];

  $pusher=new Pusher\Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET, $options);
  $pusher->trigger(PUSHER_VIDEO_CHANNEL, $event, $data);

  return successHttpResponse(['success'=>true]);
}

function saveCurrentTime($request){
  if(!isset($request['seconds'])){
    return errorHttpResponse(HTTP_INVALID_REQUEST);
  }

  $seconds=$request['seconds'];

  saveCurrentTimeToDocument($seconds);
  
  $options=['cluster'=>'eu', 'encrypted'=>true];

  $pusher=new Pusher\Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET, $options);
  $pusher->trigger(PUSHER_VIDEO_CHANNEL, PUSHER_VIDEO_CURRENT_TIME_EVENT, $request);

  return successHttpResponse(['success'=>true]);
}

/**
 * Bulk list method to stop broadcast and clear chat history
 */
function resetPageState(){
  pushEvent(['event'=>PUSHER_VIDEO_BROADCAST_END_EVENT]);
  clearHistory();
}

/********** Helpers **********/

function saveEvent($name, $data){
  $document=getPLayerDocumentContent();
  $document['event']=['event'=>$name, 'data'=>$data, 'timestamp'=>time()];

  savePlayerDocumentContent($document);
}

function saveCurrentTimeToDocument($seconds){
  $document=getPlayerDocumentContent();
  $document['currentTime']=$seconds;

  savePlayerDocumentContent($document);
}
<?php
ini_set('display_error', true);
ini_set('error_reporting', E_ALL);

require_once '../vendor/autoload.php';
require_once './methods.php';

routeRequest();

/********** API methods **********/

function connect($request){
  if(empty($request['user'])){
    return errorHttpResponse(HTTP_INVALID_REQUEST);
  }

  $excludeSocketId=isset($request['socket_id']) ? $request['socket_id'] : null;
  $user=$request['user'];

  addUser($user);

  $options=['cluster'=>'eu', 'encrypted'=>true];

  $pusher=new Pusher\Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET, $options);
  $pusher->trigger(PUSHER_CHAT_CHANNEL, PUSHER_CHAT_USER_EVENT, $user, $excludeSocketId);

  $document=getChatDocumentContent();

  return successHttpResponse($document);
}

function sendMessage($request){
  if(empty($request['user']) || empty($request['text'])){
    return errorHttpResponse(HTTP_INVALID_REQUEST);
  }

  $excludeSocketId=isset($request['socket_id']) ? $request['socket_id'] : null;

  $data=$request;
  $data['id']=md5(rand());
  $data['timestamp']=time();

  $options=['cluster'=>'eu', 'encrypted'=>true];

  $pusher=new Pusher\Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET, $options);
  $pusher->trigger(PUSHER_CHAT_CHANNEL, PUSHER_CHAT_MESSAGE_EVENT, $data, $excludeSocketId);

  addMessage($data);

  return successHttpResponse($data);
}

function getHistoryFile(){
  $messages=getMessages();
  $history=[];

  foreach($messages as $message){
    // Flatten logs array for csv format
    $message['user_id']=$message['user']['id'];
    $message['user_name']=$message['user']['name'];
    unset($message['user']);

    // Remove system fields from output
    unset($message['socket_id']);

    $history[]=arrayToCsv($message);
  }

  echo implode($history, "\n");
}

function clearHistory(){
  $options=['cluster'=>'eu', 'encrypted'=>true];
  
  $pusher=new Pusher\Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET, $options);
  $pusher->trigger(PUSHER_CHAT_CHANNEL, PUSHER_CHAT_CLEAR_HISTORY_EVENT);

  truncateChatDocument();

  return successHttpResponse(['success'=>true]);
}

/********** Helpers **********/

function getUsers(){
  $document=getChatDocumentContent();
  
  return $document['users'];
}

function addUser($user){
  $document=getChatDocumentContent();
  $userExists=false;

  foreach($document['users'] as $documentUser){
    if($documentUser['id']==$user['id']){
      $userExists=true;

      break;
    }
  }

  if(!$userExists){
    $document['users'][]=$user;

    saveChatDocumentContent($document);
  }
}

function getMessages(){
  $document=getChatDocumentContent();

  return $document['messages'];
}

function addMessage($message){
  $document=getChatDocumentContent();
  $document['messages'][]=$message;

  saveChatDocumentContent($document);
}
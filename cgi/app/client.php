<?php
ini_set('display_error', true);
ini_set('error_reporting', E_ALL);

require_once './methods.php';

routeRequest();

/********** API methods **********/

function connect(){
  $document=getPlayerDocumentContent();
  $event=$document['event'];
  $currentTime=$document['currentTime'];

  if(empty($event)){
    return successHttpResponse(null);
  }
  
  if($event['event']!==PUSHER_VIDEO_PLAY_EVENT){
    return successHttpResponse($event);
  }

  $data=$event['data'];
  $timestamp=$event['timestamp'];
  
  $data['seconds']=$currentTime;
  $event['data']=$data;

  return successHttpResponse($event);
}

function currentTime(){
  $document=getPlayerDocumentContent();
  $currentTime=$document['currentTime'];

  return successHttpResponse(['seconds'=>$currentTime]);
}
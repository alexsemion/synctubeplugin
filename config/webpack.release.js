var ExtractTextPlugin=require('extract-text-webpack-plugin'),
  config=require('./webpack.base.js'),
  webpack=require('webpack');

config.output.filename='app.[chunkhash].js';
config.output.chunkFilename='[id].[chunkhash].js';

config.plugins=(config.plugins || []).concat([
  new ExtractTextPlugin({
    filename:'[name].[hash].css'
  }),
  new webpack.DefinePlugin({
    'process.env':{
      NODE_ENV:'"production"',
      API_URL:'"http://localhost:3000"'
    }
  })
]);

module.exports=config;
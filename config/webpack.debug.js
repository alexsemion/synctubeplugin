var ExtractTextPlugin=require('extract-text-webpack-plugin'),
  config=require('./webpack.base.js'),
  webpack=require('webpack');

config.watch=true;
config.watchOptions={
  aggregateTimeout:2000,
  poll:2000
};

config.plugins=(config.plugins || []).concat([
  new ExtractTextPlugin({
    filename:'[name].css'
  }),
  new webpack.DefinePlugin({
    'process.env':{
      NODE_ENV:'"development"',
      API_URL:'"http://localhost:3000/cgi/app"'
    }
  })
]);

module.exports=config;
var ExtractTextPlugin=require('extract-text-webpack-plugin'),
  HtmlWebpackPlugin=require('html-webpack-plugin'),
  path=require('path');

module.exports={
  entry:{
    'client':['babel-polyfill', path.resolve(__dirname, '../src/js/index.client.js')],
    'admin':['babel-polyfill', path.resolve(__dirname, '../src/js/index.admin.js')]
  },
  module:{
    loaders:[
      {
        test:/\.js$/,
        include:[path.resolve(__dirname, '../src/js')],
        use:['babel-loader?cacheDirectory=true', 'eslint-loader']
      },
      {
        test:/\.jpe?g|\.svg|\.gif|\.png|\.ico$/,
        use:['file-loader?name=[name].[ext]']
      },
      {
        test:/\.css$/,
        loader:ExtractTextPlugin.extract({
          use:['css-loader']
        })
      },
      {
        test:/\.less$/,
        loader:ExtractTextPlugin.extract({
          use:['css-loader', 'less-loader']
        })
      },
      {
        test:/\.eot|\.ttf|\.woff|\.woff2?$/,
        use:['file-loader?name=[name].[ext]']
      }
    ]
  },
  output:{
    path:path.resolve(__dirname, '../public'),
    publicPath:'',
    filename:'js/[name].js'
  },
  resolve:{
    modules:[
      path.resolve(__dirname, '../node_modules')
    ]
  },
  plugins:[
    new HtmlWebpackPlugin({
      filename:'client.html',
      template:'src/index.html',
      chunks:['client']
    }),
    new HtmlWebpackPlugin({
      filename:'admin.html',
      template:'src/index.html',
      chunks:['admin']
    })
  ]
};

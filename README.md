Installation:

1. clone repo
2. from root directory - "npm install"
3. from /cgi directory - "composer install"

To run locally: 
1. localhost:3000 must watch to root directory
2. from root directory "npm run build-dev" (the process will be in pending state, do not close it)
3. open http://localhost:3000/public/client.html and http://localhost:3000/public/admin.html
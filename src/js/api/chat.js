import Http from './http.js';

export const BASE_PATH='/chat.php';
export const CONNECT='connect';
export const SEND_MESSAGE='sendMessage';
export const GET_HISTORY_FILE='getHistoryFile';

export const GET_HISTORY_FILE_URL=`${process.env.API_URL}/${BASE_PATH}?method=${GET_HISTORY_FILE}`;

const http=new Http(BASE_PATH);

export function connect(data){
  return http.post(CONNECT, data);
}

export function sendMessage(data){
  return http.post(SEND_MESSAGE, data);
}
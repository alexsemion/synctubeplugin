import Pusher from 'pusher-js';

const APP_ID='3e206b6b3bea4d4bc2ff';
const CLUSTER='eu';

const VIDEO_CHANNEL='video';
const CHAT_CHANNEL='chat';

const socket=new Pusher(APP_ID, {
  cluster:CLUSTER,
  encrypted:true
});

const videoChannel=socket.subscribe(VIDEO_CHANNEL);
const chatChannel=socket.subscribe(CHAT_CHANNEL);

export const VIDEO_BROADCAST_START_EVENT='video:broadcast.start';
export const VIDEO_BROADCAST_END_EVENT='video:broadcast.end';
export const VIDEO_PLAY_EVENT='video:play';
export const VIDEO_PAUSE_EVENT='video:pause';
export const VIDEO_CURRENT_TIME_EVENT='video:current.time';

export const CHAT_USER_EVENT='chat:user';
export const CHAT_MESSAGE_EVENT='chat:message';
export const CHAT_CLEAR_HISTORY_EVENT='chat:clear-history';

/**
 * Retrieves and returns socket id value
 *
 * @export
 * @returns {Promise}
 */
export function getSocketId(){
  if(socket.connection.state==='connected'){
    return Promise.resolve(socket.connection.socket_id);
  }
  else{
    return new Promise((resolve)=>{
      socket.connection.bind('connected', function(){
        resolve(socket.connection.socket_id);
      });
    });
  }
}

export function onVideoBroadcastStart(callback){
  return videoChannel.bind(VIDEO_BROADCAST_START_EVENT, callback);
}

export function onVideoBroadcastEnd(callback){
  return videoChannel.bind(VIDEO_BROADCAST_END_EVENT, callback);
}

export function onVideoPlay(callback){
  return videoChannel.bind(VIDEO_PLAY_EVENT, callback);
}

export function onVideoPause(callback){
  return videoChannel.bind(VIDEO_PAUSE_EVENT, callback);
}

export function onVideoCurrentTime(callback){
  return videoChannel.bind(VIDEO_CURRENT_TIME_EVENT, callback);
}

export function onChatUser(callback){
  return chatChannel.bind(CHAT_USER_EVENT, callback);
}

export function onChatMessage(callback){
  return chatChannel.bind(CHAT_MESSAGE_EVENT, callback);
}

export function onChatClearHistory(callback){
  return chatChannel.bind(CHAT_CLEAR_HISTORY_EVENT, callback);
}
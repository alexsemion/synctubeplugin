import axios from 'axios';

import formUrlEncoded from 'form-urlencoded';

export default class Http{
  axiosInstance=null;

  constructor(basePath){
    this.axiosInstance=axios.create({
      baseURL:`${process.env.API_URL}${basePath}`,
      timeout:30000,
      headers:{
        ['Content-Type']:'application/x-www-form-urlencoded'
      }
    });
  }

  get(method, queryParams={}, config={}){
    config.params={method, ...queryParams};

    return this.formatResponse(this.axiosInstance.get('', config));
  }

  post(method, body={}, config={}){
    config.params={method, ...config.params};

    return this.formatResponse(this.axiosInstance.post('', formUrlEncoded(body), config));
  }

  formatResponse(request){
    return request.then(
      (response)=>{
        return response.data;
      },
      (error)=>{
        const errorData=error.response ? error.response.data : {};

        return Promise.reject({
          ...errorData,
          __response:error
        });
      }
    );
  }
}
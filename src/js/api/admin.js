import Http from './http.js';

export const BASE_PATH='/admin.php';
export const PUSH_EVENT='pushEvent';
export const SAVE_CURRENT_TIME='saveCurrentTime';
export const RESET_PAGE_STATE='resetPageState';

const http=new Http(BASE_PATH);

export function pushEvent(event){
  return http.post(PUSH_EVENT, event);
}

export function saveCurrentTime(seconds){
  return http.post(SAVE_CURRENT_TIME, {seconds});
}

export function resetPageState(){
  const xhr=new XMLHttpRequest();

  xhr.open('POST', `${process.env.API_URL}${BASE_PATH}?method=${RESET_PAGE_STATE}`, false);
  xhr.send();
}
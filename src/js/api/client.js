import Http from './http.js';

export const BASE_PATH='/client.php';
export const CONNECT='connect';
export const CURRENT_TIME='currentTime';

const http=new Http(BASE_PATH);

export function connect(){
  return http.get(CONNECT);
}

export function currentTime(){
  return http.get(CURRENT_TIME);
}
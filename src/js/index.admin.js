import '../less/index.less';

import React from 'react';
import ReactDOM from 'react-dom';

import Admin from '~/components/admin';

ReactDOM.render(<Admin />, document.getElementById('app'));
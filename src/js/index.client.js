import '../less/index.less';

import React from 'react';
import ReactDOM from 'react-dom';

import Client from '~/components/client';

ReactDOM.render(<Client />, document.getElementById('app'));
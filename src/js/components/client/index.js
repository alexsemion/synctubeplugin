import autobind from 'autobind-decorator';
import MobileDetect from 'mobile-detect';
import React, {PureComponent} from 'react';
import {parse} from 'url';

import {connect, currentTime} from '~/api/client.js';
import {
  VIDEO_PLAY_EVENT,
  VIDEO_PAUSE_EVENT,
  VIDEO_BROADCAST_END_EVENT,
  onVideoBroadcastEnd,
  onVideoPlay,
  onVideoPause,
  onVideoCurrentTime
} from '~/api/pusher.js';

import YouTubePlayer from '~/components/youtube-player';

import Chat from '~/components/chat';

const MAX_VIDEO_DESYNC_TIME=2;

const md=new MobileDetect(window.navigator.userAgent);

/**
 * Whether current device is mobile or not
 * TODO: add iPad is hack need better way to determine
 * 
 * @type {bool}
 */
const isMobileDevice=md.phone()!==null || navigator.userAgent.match(/iPad/i)!=null;

export default class Client extends PureComponent{
  /**
   * Player api instance
   *
   * @memberof Client
   */
  playerApi=null;

  /**
   * Previously triggered event name
   * 
   * @memberof Client
   */
  lastEvent=null;

  onPlayerReady=function(){};

  state={
    videoId:null,
    isVideoEnd:false,

    showStartPlayCap:false,
    isPaused:false,
    isPlayerReady:false
  };

  async componentDidMount(){
    onVideoBroadcastEnd(this.onBroadcastEnd);
    onVideoPlay(this.onPlay);
    onVideoPause(this.onPause);
    onVideoCurrentTime(this.onCurrentTime);

    const event=await connect();

    if(!event){
      return ;
    }

    const {event:eventName, data}=event;

    // Connect method returns last emitted event, so trigger it artificially
    switch(eventName){
      case VIDEO_PLAY_EVENT:
        this.onPlay(data);

        break;

      case VIDEO_PAUSE_EVENT:
        this.onPause(data);

        break;
    }
  }

  render(){
    const {videoId, isVideoEnd, isPaused, isPlayerReady, showStartPlayCap}=this.state;

    return (
      <div className="client-page">
        <div className="client-page__widget">
          {
            !isVideoEnd && videoId===null && (
              <div className="client-page__player-cap">
                Das Neudorff Webinar hat noch nicht begonnen, oder ist bereits zu Ende
              </div>
            )
          }

          {
            isVideoEnd && videoId===null && (
              <div className="client-page__player-cap client-page__player-cap_video-end">
                Vielen Dank für Ihre Aufmerksamkeit.
              </div>
            )
          }

          {
            videoId!==null && (
              <div className="client-page__player">
                {
                  showStartPlayCap && (
                    <div className="client-page__player-cap client-page__player-cap_absolute">
                      Bitte drücken Sie den Start-Button, um das Webinar zu starten.

                      <button
                        className="button client-page__cap-button"
                        disabled={!isPlayerReady || isPaused}
                        onClick={this.onStartButtonClick}
                      >
                        Start
                      </button>
                    </div>
                  )
                }

                <YouTubePlayer
                  apiRef={ref=>this.playerApi=ref}
                  className="client-page__embedded"
                  onReady={this.onReady}
                  videoId={videoId}
                />
              </div>
            )
          }

          <Chat className="client-page__chat" user={this.getChatUser()} />
        </div>
      </div>
    );
  }

  getChatUser(){
    const defaultUserId=Math.ceil(Math.random()*1000);
    const defaultUserName=`Guest ${defaultUserId}`;

    let {query:{user_id, user_name}}=parse(location.search, true);

    if(user_id!==undefined){
      user_id=Number(user_id);
    }

    return {id:user_id || defaultUserId, name:user_name || defaultUserName};
  }

  @autobind
  onBroadcastEnd(data){
    console.log('PUSHER_EVENT_BROADCAST_END', data);

    this.lastEvent=VIDEO_BROADCAST_END_EVENT;
    this.setState({videoId:null, isPaused:false, isVideoEnd:data && Number(data.end)});
  }

  @autobind
  onPlay(data){
    console.log('PUSHER_EVENT_PLAY', data);

    this.setState({isPaused:false});

    const seekAndPlay=()=>{
      this.playerApi.seekTo(Number(data.seconds));
      this.playerApi.playVideo();
    };

    if(this.state.videoId==data.videoId){
      seekAndPlay();
    }
    else{
      this.setState({videoId:data.videoId}, seekAndPlay);
    }

    // If last event is pause event - video will start on mobile devices
    if((!this.lastEvent || this.lastEvent===VIDEO_BROADCAST_END_EVENT) && isMobileDevice){
      this.setState({showStartPlayCap:true});
    }

    this.lastEvent=VIDEO_PLAY_EVENT;
  }

  @autobind
  onPause(data){
    console.log('PUSHER_EVENT_PAUSE', data);

    const pause=()=>{
      // IMPORTANT: For some reason pause does not work after seek function call.
      // this.playerApi.seekTo(Number(data.seconds)).then(()=>{
      //   this.playerApi.pauseVideo();
      // });

      this.playerApi.pauseVideo();
    };

    this.setState({isPaused:true});

    if(this.state.videoId==data.videoId){
      this.playerApi.pauseVideo();
    }
    else{
      this.onPlayerReady=()=>{
        this.playerApi.pauseVideo();
      };

      this.setState({videoId:data.videoId}, pause);
    }

    if(this.lastEvent!==null){
      this.lastEvent=VIDEO_PAUSE_EVENT;
    }
  }

  @autobind
  async onCurrentTime(data){
    console.log('PUSHER_EVENT_CURRENT_TIME', data);

    if(!this.playerApi){
      return ;
    }

    const playerCurrentTime=await this.playerApi.getCurrentTime();

    if(Math.abs(playerCurrentTime-data.seconds)>=MAX_VIDEO_DESYNC_TIME){
      this.playerApi.seekTo(Number(data.seconds));
    }
  }

  @autobind
  onReady(){
    this.setState({isPlayerReady:true});

    this.onPlayerReady();
    this.onPlayerReady=function(){};
  }

  @autobind
  async onStartButtonClick(){
    this.setState({showStartPlayCap:false});
    this.playerApi.playVideo();

    const {seconds}=await currentTime();

    this.playerApi.seekTo(Number(seconds));
  }
}
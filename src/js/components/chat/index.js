import autobind from 'autobind-decorator';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';

import {connect, sendMessage} from '~/api/chat.js';
import {getSocketId, onChatUser, onChatMessage, onChatClearHistory} from '~/api/pusher.js';

export default class Chat extends PureComponent{
  static propTypes={
    user:PropTypes.shape({
      id:PropTypes.number,
      name:PropTypes.string
    }).isRequired,
    displayUserList:PropTypes.bool
  };

  static defaultProps={
    displayUserList:false
  };

  /**
   * Socket id of pusher
   */
  pusherSocketId=null;

  /**
   * Instance of chat messages element
   *
   * @memberof Chat
   */
  messagesRef=null;

  state={
    users:[],
    messages:[],

    /**
     * Whether text input element is disabled
     * 
     * @type {bool}
     */
    inputDisabled:true,

    /**
     * Currently inputted message's text value
     * 
     * @type {string}
     */
    inputText:''
  };

  async componentDidMount(){
    const {user}=this.props;
    this.pusherSocketId=await getSocketId();

    const {users, messages}=await connect({user, socket_id:this.pusherSocketId});

    this.setState({users, messages, inputDisabled:false}, ()=>{
      this.scrollChatToBottom();
    });

    onChatUser((newUser)=>{
      console.log('CHAT_USER_EVENT', newUser);

      let users=[...this.state.users];
      users.push(newUser);

      this.setState({users});
    });

    onChatMessage((newMessage)=>{
      console.log('CHAT_MESSAGE_EVENT', newMessage);

      this.pushMessageToState(newMessage);
    });

    onChatClearHistory(()=>{
      console.log('CHAT_CLEAR_HISTORY_EVENT');

      this.setState({messages:[]});
    });
  }

  render(){
    const {className, user:currentUser, displayUserList}=this.props;
    const {users, messages, inputDisabled, inputText}=this.state;

    return (
      <div className={classNames(className, 'chat')}>
        <div className="chat__header">Chat</div>

        <div className="chat__body">
          <div className="chat__content">
            <div className="chat__messages" ref={el=>this.messagesRef=el}>
              {
                messages.map((message)=>{
                  const {id, user, text}=message;

                  const isCurrentUser=currentUser.id==user.id;

                  return (
                    <div
                      className={classNames('chat__message chat-message', {'chat__message_right':isCurrentUser})}
                      key={id}
                    >
                      <div className="chat-message__username">{user.name}</div>
                      <div className="chat-message__text">{text}</div>
                    </div>
                  );
                })
              }
            </div>

            <form className="chat__form" onSubmit={this.onSubmit}>
              <input
                className="chat__input"
                onChange={this.onInputTextChange}
                placeholder="Geben Sie hier Ihre Frage ein"
                readOnly={inputDisabled}
                type="text"
                value={inputText}
              />
            </form>
          </div>

          {
            displayUserList && (
              <div className="chat__users">
                {
                  users.map((user)=>{
                    const {id, name}=user;

                    return (
                      <div className="chat__user" key={id}>
                        {id}, {name}
                      </div>
                    );
                  })
                }
              </div>
            )
          }
        </div>
      </div>
    );
  }

  async sendMessage(){
    const {user}=this.props;
    const text=this.state.inputText.trim();

    if(text.length===0){
      return ;
    }

    this.setState({inputText:''});

    const message=await sendMessage({user, text, socket_id:this.pusherSocketId});

    this.pushMessageToState(message);
  }

  /**
   * Scrolls chat element to bottom
   * 
   * @memberof Chat
   */
  scrollChatToBottom(){
    this.messagesRef.scrollTop=this.messagesRef.scrollHeight;
  }

  /**
   * Pushes message object to state messages array
   * 
   * @param {Object} newMessage. Message object to add
   * @param {function} callback. Callback function to call after re-render
   * @memberof Chat
   */
  pushMessageToState(newMessage){
    let messages=[...this.state.messages];

    messages.push(newMessage);
    messages.sort((a, b)=>a.timestamp-b.timestamp);

    this.setState({messages}, ()=>{
      this.scrollChatToBottom();
    });
  }

  @autobind
  onInputTextChange(event){
    this.setState({inputText:event.target.value});
  }

  @autobind
  onSubmit(event){
    event.preventDefault();

    this.sendMessage();
  }
}
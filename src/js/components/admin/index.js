import autobind from 'autobind-decorator';
import classNames from 'classnames';
import React, {PureComponent} from 'react';

import {pushEvent, saveCurrentTime, resetPageState} from '~/api/admin.js';
import {GET_HISTORY_FILE_URL} from '~/api/chat.js';

import {
  VIDEO_BROADCAST_START_EVENT,
  VIDEO_BROADCAST_END_EVENT,
  VIDEO_PLAY_EVENT,
  VIDEO_PAUSE_EVENT
} from '~/api/pusher.js';

import Chat from '~/components/chat';

import YouTubePlayer, {PLAYER_STATE_PLAYING} from '~/components/youtube-player';

const TIME_TRACKER_FREQUENCY=3000;

export default class Admin extends PureComponent{
  /**
   * Whether player has enabled live or not
   * 
   * @type {boolean}
   * @memberof Admin
   */
  isPlayerLive=false;

  /**
   * Instance of player element
   * 
   * @memberof Admin
   */
  playerRef=null;

  /**
   * Instance of player API
   * 
   * @memberof Admin
   */
  playerApi=null;

  /**
   * Time tracker timer instance
   * 
   * @memberof Admin
   */
  timeTrackerTimer=null;

  state={
    videoId:null
  };

  componentDidMount(){
    // When page is unloaded - dispatch broadcast-end event
    window.addEventListener('beforeunload', this.onWindowUnload);

    this.startPlayerTimeTracker();
  }

  componentWillUnmount(){
    window.removeEventListener('beforeunload', this.onWindowUnload);

    this.stopPlayerTimeTracker();
  }

  render(){
    const {videoId}=this.state;
    const user={id:-1, name:'Neudorff Berater'};

    return (
      <div className="admin-page">
        <form
          className={classNames('admin-page__form video-form', {'admin-page__form_shifted':videoId!==null})}
          onSubmit={this.onFormSubmit}
        >
          <input className="video-form__input input" />
          <button className="video-form__submit button" type="submit">Find</button>
        </form>

        {
          videoId!==null && (
            <YouTubePlayer
              controls
              apiRef={api=>this.playerApi=api}
              className="admin-page__player"
              onLiveOn={this.onPlayerLiveOn}
              onLiveOff={this.onPlayerLiveOff}
              onPlay={this.onPlayerPlay}
              onPause={this.onPlayerPause}
              ref={el=>this.playerRef=el}
              videoId={videoId}
            />
          )
        }

        <a
          className="admin-page__save-chat-button button"
          download="chat-history.csv"
          href={GET_HISTORY_FILE_URL}
          type="button"
        >
          Save chat
        </a>

        <Chat className="admin-page__chat" displayUserList={true} user={user} />
      </div>
    );
  }

  @autobind
  onFormSubmit(event){
    event.preventDefault();

    const videoId=event.target.elements[0].value;

    this.setState({videoId});
  }

  // TODO: add previous request (if any) cancel when new event arrives

  @autobind
  async onPlayerLiveOn(){
    this.isPlayerLive=true;

    pushEvent({event:VIDEO_BROADCAST_START_EVENT});

    // If video is already playing - trigger play event
    if(this.playerRef.playerState===PLAYER_STATE_PLAYING){
      pushEvent({event:VIDEO_PLAY_EVENT, data:await this.playerRef.getPlayerStateData()});
    }
    else{
      this.playerApi.playVideo();
    }
  }

  @autobind
  onPlayerLiveOff(end=false){
    this.isPlayerLive=false;

    pushEvent({event:VIDEO_BROADCAST_END_EVENT, data:{end:Number(end)}});

    this.playerApi.pauseVideo();
  }

  @autobind
  onPlayerPlay(data){
    if(!this.isPlayerLive){
      return ;
    }

    pushEvent({event:VIDEO_PLAY_EVENT, data});
  }

  @autobind
  onPlayerPause(data){
    if(!this.isPlayerLive){
      return ;
    }

    pushEvent({event:VIDEO_PAUSE_EVENT, data});
  }

  @autobind
  onWindowUnload(){
    resetPageState();
  }

  @autobind
  async startPlayerTimeTracker(){
    if(this.isPlayerLive && this.playerRef.playerState===PLAYER_STATE_PLAYING){
      const currentTime=await this.playerApi.getCurrentTime();

      saveCurrentTime(currentTime);
    }

    this.timeTrackerTimer=setTimeout(this.startPlayerTimeTracker, TIME_TRACKER_FREQUENCY);
  }

  stopPlayerTimeTracker(){
    clearTimeout(this.timeTrackerTimer);
  }
}
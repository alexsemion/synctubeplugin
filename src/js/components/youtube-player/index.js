import autobind from 'autobind-decorator';
import classNames from 'classnames';
import moment from 'moment';
import 'moment-duration-format';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';

import YouTube from 'react-youtube';

export const PLAYER_STATE_PLAYING=1;

export default class YouTubePlayer extends PureComponent{
  static propTypes={
    videoId:PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ]).isRequired,
    controls:PropTypes.bool,
    apiRef:PropTypes.func,
    onReady:PropTypes.func,
    onLiveOn:PropTypes.func,
    onLiveOff:PropTypes.func,
    onPlay:PropTypes.func,
    onPause:PropTypes.func,
    onSeek:PropTypes.func,
    onEnd:PropTypes.func
  };

  static defaultProps={
    controls:false,
    apiRef:function(){},
    onReady:function(){},
    onLiveOn:function(){},
    onLiveOff:function(){},
    onPlay:function(){},
    onPause:function(){},
    onSeek:function(){},
    onEnd:function(){}
  };

  /**
   * Player element reference
   *
   * @memberof Client
   */
  playerRef=null;

  /**
   * Player's current state
   * 
   * @memberof YouTubePlayer
   */
  playerState=undefined;

  /**
   * Time count down timer instance
   * 
   * @memberof YouTubePlayer
   */
  countDownTimer=null;

  state={
    isLive:false,
    currentTime:0,
    totalTime:0
  };

  async componentDidMount(){
    this.props.apiRef(this.playerRef.internalPlayer);

    const totalTime=await this.playerRef.internalPlayer.getDuration();

    this.setState({totalTime:totalTime*1000});
  }

  componentWillUnmount(){
    clearTimeout(this.countDownTimer);
  }

  render(){
    const {className, controls, videoId, onReady}=this.props;
    const {isLive, currentTime, totalTime}=this.state;

    const formatCurrentTime=moment.duration(currentTime).format('m:ss', {trim:false});
    const formatTotalTime=moment.duration(totalTime).format('m:ss', {trim:false});

    const options={
      playerVars:{
        autoplay:0,
        controls:0,
        disablekb:1,
        iv_load_policy:3,
        modestbranding:1,
        rel:0,
        showinfo:0,
        playsinline:1
      }
    };

    return (
      <div className={classNames('video-player', className)}>
        <YouTube
          className="video-player__embed"
          onEnd={this.onEnd}
          onPlay={this.onPlay}
          onPause={this.onPause}
          onReady={onReady}
          onStateChange={this.onStateChange}
          opts={options}
          ref={el=>this.playerRef=el}
          videoId={videoId}
        />

        <div className="video-player__time">
          {formatCurrentTime} / {formatTotalTime}
        </div>

        {
          controls && (
            <div className="video-player__controls">
              <button
                className={classNames('video-player__control button', {'button_red':isLive})}
                onClick={this.onLiveClick}
                type="button"
              >
                Live ({isLive ? 'On' : 'Off'})
              </button>

              <button className="video-player__control button" onClick={this.onPlayClick} type="button">Play</button>
              <button className="video-player__control button" onClick={this.onPauseClick} type="button">Pause</button>
            </div>
          )
        }
      </div>
    );
  }

  @autobind
  async onPlay(){
    this.props.onPlay(await this.getPlayerStateData());

    this.startTimeCountDown();
  }

  @autobind
  async onPause(){
    this.props.onPause(await this.getPlayerStateData());

    this.pauseTimeCountDown();
  }

  @autobind
  async onEnd(){
    const {onEnd, onLiveOff}=this.props;

    this.pauseTimeCountDown();

    this.setState({isLive:false});

    onEnd(await this.getPlayerStateData());
    onLiveOff(true);
  }

  @autobind
  onStateChange(event){
    this.playerState=event.data;
  }

  /**
   * Returns player's state data object
   * 
   * @returns 
   * @memberof YouTubePlayer
   */
  async getPlayerStateData(){
    const {videoId}=this.props;

    return {
      videoId,
      seconds:await this.playerRef.internalPlayer.getCurrentTime()
    };
  }

  @autobind
  onLiveClick(){
    const {onLiveOn, onLiveOff}=this.props;
    const {isLive}=this.state;

    isLive ? onLiveOff() : onLiveOn();

    this.setState({isLive:!isLive});
  }

  @autobind
  onPlayClick(){
    this.playerRef.internalPlayer.playVideo();
  }

  @autobind
  onPauseClick(){
    this.playerRef.internalPlayer.pauseVideo();
  }

  @autobind
  async startTimeCountDown(){
    if(!this.playerRef){
      return ;
    }

    const currentTime=await this.playerRef.internalPlayer.getCurrentTime();

    this.setState({currentTime:currentTime*1000});

    this.countDownTimer=setTimeout(this.startTimeCountDown, 1000);
  }

  pauseTimeCountDown(){
    clearTimeout(this.countDownTimer);
  }
}